var Main = function () {
	
	//var ajaxCheckNotificaciones;
	
	var handleMainMenuPosition = function(){

		var menutopposition = $('#nav-principal').offset().top; // - $('.navbar.navbar-fixed-top').innerHeight();
		
		$(window).scroll(function (event) {
			var scrollPos = $(window).scrollTop();
			// Do something


			if (scrollPos > menutopposition){
				$('body').css('padding-top', $('#nav-principal').innerHeight());
				$('#nav-principal').addClass('menu-fixed');
			} else {
				$('#nav-principal').removeClass('menu-fixed');
				$('body').css('padding-top', '0');
			}
		});	



	}


	var handleOffCanvasMenu = function(){


		 $(document).on("click",function(e) {

		 		if ($('body').hasClass('offcanvas')){


			        var container = $("#offcanvas");
			        var btn = $('#small-button');

		            if (!container.is(e.target) && !btn.is(e.target) && container.has(e.target).length === 0) { 
		            //Se ha pulsado en cualquier lado fuera de los elementos contenidos en la variable container
						$('#offcanvas').removeClass('active');
						$('body').removeClass('offcanvas');		

		            }
		        }
		   });


		 $('[data-toggle="offcanvas"]').click(function () {
			$('#offcanvas').toggleClass('active');
			$('body').toggleClass('offcanvas');
		  });

	}


	var readURL = function(input) {

	  if (input.files && input.files[0]) {
	    var reader = new FileReader();

	    reader.onload = function(e) {
	      	$('#imgLogoPreview').attr('src', e.target.result);
		    $('#msgActual').addClass('d-none');
		    $('#msgSeleccionada').removeClass('d-none');
		    $('button.btn-handle-logo').html('Cambiar de archivo');
			$('#inputLogoEliminar').val('');

	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}


	var handleImagePreview = function(){

		$('.btn-handle-logo, #imgLogoPreview').click(function(e){
			e.preventDefault();
			$('#customLogo').trigger('click');
		});
		
		$("#customLogo").change(function() {
		  readURL(this);
		});		

		$('#btnEliminarLogo').click(function(e){
			e.preventDefault();	
			$('#inputLogoEliminar').val('1');
			$('#imgLogoPreview').attr('src', imgDefault);
		    $('#msgActual').removeClass('d-none').html('Este anuncio no tiene logo.');
		    $('#msgSeleccionada').addClass('d-none');
		    $('button.btn-handle-logo').html('Seleccione un logo');
		});
	}


	var handleFormIngreso = function(){

		$('.frmIngreso').validate({
			  	rules: {
				    // simple rule, converted to {required:true}
				    pwd: "required",
				    // compound rule
				    log: {
				      required: true,
				      email: true
				    }
			  	},
			  	messages: {
				    pwd: "Por favor, ingrese contraseña",
				    log: {
				      required: "Ingrese el email",
				      email: "El formato del email debe ser del estilo nombre@dominio.com"
				    }
				}


		});

	}

	var handleFormRegistro = function(){

		$('.frmRegistro').validate({
			  	rules: {
				    // simple rule, converted to {required:true}
				    nombre: "required",
				    // compound rule
				    email: {
				      required: true,
				      email: true
				    },
				    password: {
				    	required: true
				    },
				    'password-repetir': {
				    	required: true,
				    	equalTo: "#password"
				    }
			  	},
			  	messages: {
				    nombre: "Por favor, ingrese nombre",
				    email: {
				      required: "Ingrese el email",
				      email: "El formato del email debe ser del estilo nombre@dominio.com"
				    },
				    password: "Por favor, ingrese contraseña",
				    'password-repetir': {
				      required: "Repita la contraseña",
				      equalTo: "No coinciden las contraseñas"
				    }
				}


		});

	}

	var handleFormEdicion = function(){

		$('.frmEdicion').validate({
			  	rules: {
				    // simple rule, converted to {required:true}
				    nombre: "required",
				    // compound rule
				    correo_electronico: {
				      email: true
				    },
				    customLogo: {
				    	extension: "jpg|jpeg|bmp|png|gif|svg"
				    }
			  	},
			  	messages: {
				    nombre: "Por favor, ingrese título",
				    correo_electronico: {
				      email: "El formato del email debe ser del estilo nombre@dominio.com"
				    },
				    customLogo: {
				    	extension: "Formatos aceptados jpg, jpeg, bmp, png, gif y svg"
				    }
				},
				errorPlacement: function(error, element) {
					//alert(element.attr("name"));
					//var placement = $(element).data('error');
					if (element.attr("name") == 'customLogo') {
						$('.errorLogo').append(error);
					} 
			    }


		});

	}

	return{
		init: function(){
			handleMainMenuPosition();
			handleOffCanvasMenu();
			handleImagePreview();
			handleFormIngreso();
			handleFormRegistro();
			handleFormEdicion();
		}
	}

}();