<footer class="container-fluid cace-main-container bg-blue" id="footer">
  <div class="text-right">
    <div class="d-none d-lg-block">
      <div class="logo pull-left"></div>
      <h5>
        <i class="fa fa-envelope-o"> </i> estetica@caceglobal.org
        <i class="fa fa-phone"> </i> +54 011 5778 7878
        <i class="fa fa-home"> </i> Carlos Pellegrini 651 4° piso. (C1009ABM). Ciudad Autónoma de Bs. As., Argentina.
      </h5>
    </div>
    <div class="d-lg-none footer-mobile text-center">
      <h5><div class="logo pull-left"></div></h5>
      <h5><i class="fa fa-envelope-o"> </i> estetica@caceglobal.org</h5>
      <h5><i class="fa fa-phone"> </i> +54 011 5778 7878</h5>
      <h5><i class="fa fa-home"> </i> Carlos Pellegrini 651 4° piso. (C1009ABM). Ciudad Autónoma de Bs. As., Argentina.</h5>
    </div>
  </div><!-- .col-md-16 -->

  <div class="d-none d-md-block" id="footer-menu">
    <div class="row">
      <div class="col">
        <h5>UBICACIÓN</h5>
        <hr>
        <a target="_blank" href="<?php echo cace_global_url(); ?>/contacto"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mapa.PNG" width="100%" height="auto" title="Consejo Argentino de Ciencias Estéticas" alt="Consejo Argentino de Ciencias Estéticas"></a>
      </div>

      <div class="col">
        <h5>INSTITUCIONAL</h5>
        <hr>
        <ul class="list-unstyled">
          <li><a href="http://www.caceglobal.org/micace">MI CACE</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/institucional">Institucional</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/convenios">Convenios</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/normativas">Normativas</a></li>
        </ul>
      </div>

      <div class="col">
        <h5>CONTACTO</h5>
        <hr>
        <ul class="list-unstyled">
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/coordinacion">Coordinación</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/soporte-tecnico">Soporte técnico</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/comercial-empresas">Comercial empresas
          </a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/comision-directiva">Comisión directiva</a></li>
        </ul>
      </div>

      <div class="col">
        <h5>FAQS</h5>
        <hr>
        <ul class="list-unstyled">
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/modalidad-de-estudio">Modalidad de estudio</a></li>
          <li><a target="_blank" href="<?php echo cace_global_url(); ?>/certificaciones">Certificaciones</a></li>
        </ul>
      </div>

      <div class="col-md-3">

        <h5>NEWSLETTER</h5>
        <hr>
        <!--
        <form role="newsletter" method="get" class="search-form form-inline">
          <div class="form-group search">
            <input type="search" class="search-field form-control" placeholder="Ingresá tu email" value="" name="s" title="Ingresá tu email" data-toggle="modal" data-target="#form-newsletter">
          </div>
          <!--<input type="submit" class="search-submit btn btn-default" value="">-->
        </form>
        <a href="http://www.tomorrow.com.ar" title="Tomorrow estudio digital" target="_blank"><img src="http://tomorrow.com.ar/img/tmrow.svg" style="margin-top:80px; width:100px; text-align:right;"></a>
      </div>


    </div><!-- .row -->
  </div>

</footer><!-- container -->
</div><!-- #page -->

<a id="btnSlideup" href="#page" class="d-none d-md-block"><i class="fa fa-chevron-circle-up"></i></a>
<!-- Button trigger modal -->

<?php wp_footer(); ?>

<script>   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-25330253-1', 'auto');
  ga('require', 'GTM-WHT254B');
  ga('send', 'pageview');
</script>


<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
  
  var imgDefault = '<?php echo imagen_default('listado'); ?>';

  $(function(){
    Main.init();
  });

</script>

</body>
</html>