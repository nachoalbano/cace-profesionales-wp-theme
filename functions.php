<?php
define('WPHOST_DIR', get_template_directory());

require_once( WPHOST_DIR . '/include/bootstrap_menu.php');
require_once( WPHOST_DIR . '/include/custom-fields.php');
//require_once( WPHOST_DIR . '/include/ajaxcalls.php');
require_once( WPHOST_DIR . '/include/postcalls.php');
// require_once( WPHOST_DIR . '/include/Quality_Database.php');
//require_once( WPHOST_DIR . '/include/Quality_Session.php');
//require_once( WPHOST_DIR . '/include/webservice.php');
// require_once( WPHOST_DIR . '/include/admin.php');
require_once( WPHOST_DIR . '/include/custom-pages.php');


function cace_global_url(){
    return 'https://caceglobal.org';
}

// ver pagination
function cace_pagination($wp_query) {
    
      
    $big = 999999999; // need an unlikely integer
    $menuPager = "<div>";
    $menuPager .= paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'type' => 'list',
        'next_text' => '&raquo;',
        'prev_text' => '&laquo;',
    ));
    $menuPager .= "</div>";

    $menuPager = str_replace('page-numbers', 'page-numbers pagination', $menuPager);

    echo $menuPager;
}

register_nav_menu('primary', 'Primary Menu');
register_nav_menu('secondary', 'Secondary Menu');


function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function get_page_url($slug){
    return get_permalink( get_page_by_path( $slug ) );
}

function quality_addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

// ELIMINAR LA BARRA DEL FRONT PARA TODOS LOS USUARIOS
add_filter( 'show_admin_bar', '__return_false' );

function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            width: auto !important;
            background-image: url("<?php echo get_stylesheet_directory_uri();?>/assets/img/logo-admin.png") !important;
            background-size: auto !important;
            height: 70px !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


/* FUNCIONES para el manejo de imagens */

if ( function_exists( 'add_theme_support' ) )
    add_theme_support( 'post-thumbnails' );

function imagen_default($thumb_type = ''){
    switch ($thumb_type) {
        case 'listado':
            return 'https://dummyimage.com/110x110/cccccc/fff.png';
        break;

        case 'ficha':
            return 'https://dummyimage.com/330x110/cccccc/fff.png';
        break;

        default:
            return get_template_directory_uri().'/assets/img/imagen-default.jpg';
        break;
    }
    
}

// TAMAÑO DE IMAGENES
add_image_size( 'thumb-listado', 110, 110, false );
add_image_size( 'thumb-ficha', 400, 9999, false );

function get_thumb($post_id, $thumb_type = 'listado'){
    $thumbID = get_post_thumbnail_id( $post_id );
    if ($imgDestacada = wp_get_attachment_url( $thumbID )){
        $image = wp_get_attachment_image_src( $thumbID, 'thumb-'.$thumb_type );

        return $image[0];
    } else {
        return imagen_default($thumb_type);
    }
}

function get_thumb_id($post_id){
    $thumbID = get_post_thumbnail_id( $post_id );
    return $thumbID;
}
/*
add_image_size( 'thumb-galeria', 500, 375, true );
add_image_size( 'thumb-home-noticia', 350, 350, true );
add_image_size( 'thumb-galeria-instalaciones', 9999, 400, false );
//add_image_size( 'thumb-cuadradito', 55, 55, true );
*/

/* LOGIN ERRORS */
add_action('wp_login_failed', 'cace_front_end_login_fail');
function cace_front_end_login_fail($username){

    $referrer = get_page_url('ingreso');//(isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
    $referrer = add_query_arg('result', 'failed', $referrer);
    $referrer = add_query_arg('username', $username, $referrer);


    if(!empty($referrer) && !strstr($referrer, 'wp-admin')) :
        wp_redirect($referrer);
        exit;
    endif;
}

add_filter("login_redirect", "custom_login_redirect", 10, 3);
function custom_login_redirect( $redirect_to, $request, $user )
{
    global $user;
    if( isset( $user->roles ) && is_array( $user->roles ) ) {
        if( in_array( "administrator", $user->roles ) ) {
            return $redirect_to;
        } else {
            return get_url_anuncio_usuario($user);// get_page_url('bienvenida');
        }
    } else {
        return $redirect_to;
    }
}


add_action( 'wp_authenticate' , 'check_custom_authentication' );
function check_custom_authentication ( $username ) {

    global $wpdb;    

    if ( ! username_exists( $username ) ) {
        return;
    }

    $userinfo = get_user_by( 'login', $username );
    $activo = get_user_meta( $userinfo->ID, 'activo' ); 

    
    if(in_array( 'author', (array) $userinfo->roles ) ){
        if ( ( !$activo ) || ( $activo[0] == 'NO' ) ) {
            //$wp_error->add('error', $error);

            $referrer = get_page_url('ingreso');//(isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
            $referrer = add_query_arg('result', 'failed', $referrer);
            $referrer = add_query_arg('username', $username, $referrer);

            wp_redirect($referrer);
            exit;
        }
    }

}


function get_url_anuncio_usuario($user = null){

    if (!is_user_logged_in() && empty($user)){
        return home_url(); 
    } else {
        
        $current_user = (empty($user))?wp_get_current_user():$user;

        $args = array(
            'author'        =>  $current_user->ID,
            'orderby'       =>  'post_date',
            'order'         =>  'DESC',
            'posts_per_page' => 20,
            'post_status' => array('publish', 'pending')
        );        

        $allPost = get_posts($args);      

        if (count($allPost) == 0){
            return get_page_url('bienvenida');
        } else {
            $ultimoPost = current($allPost);

            return get_the_permalink($ultimoPost->ID);
            

        }
    }
}

function get_edit_url(){
    return get_page_url('fichaempresaedicion');
}

function get_anuncio_id(){
    if (!is_user_logged_in()){
        return false; 
    } else {
        $current_user = wp_get_current_user();

        $args = array(
            'author'        =>  $current_user->ID,
            'orderby'       =>  'post_date',
            'order'         =>  'DESC',
            'posts_per_page' => 20,
            'post_status' => array('publish', 'pending')
        );        

        $allPost = get_posts($args);      

        if (count($allPost) == 0){
            return false;
        } else {
            $ultimoPost = current($allPost);
            return $ultimoPost->ID;
        }
    }

}

/* Guardar los datos del anuncio de un usuairo */
add_action( 'admin_post_guardar_anuncio', 'cace_guardar_anuncio' );
add_action( 'admin_post_nopriv_guardar_anuncio', 'cace_guardar_anuncio' );

function cace_guardar_anuncio() {
    status_header(200);

    $anuncio_id = intval($_POST['current_post_id']);

    $nuevo_anuncio = ($anuncio_id == 0)?true:false;


    $empresa_imagen_id = intval($_POST['imagen_id']);

    $empresa_nombre = trim($_POST['nombre']);
    $empresa_telefono = trim($_POST['telefono']);
    $empresa_direccion = trim($_POST['direccion']);
    $empresa_sitio_web = trim($_POST['sitio_web']);
    $empresa_correo_electronio = trim($_POST['correo_electronico']);
    $response = $_POST["g-recaptcha-response"];

    $empresa_contenido = $_POST['content'];
    $empresa_servicios = $_POST['servicios'];

    $empresa_categoria_id = intval($_POST['categoria_id']);

    /*
    echo '<pre>';
    print_r($_POST);
    die();
    */

    $eliminar_logo = (isset($_POST['inputLogoEliminar']) && (intval($_POST['inputLogoEliminar']) == 1))?true:false;

    $current_user = wp_get_current_user();


    $argumentos = array(
        'ID' => $anuncio_id,
        'post_type' => 'post',
        'post_title' => $empresa_nombre,
        'post_content' => $empresa_contenido,
        'post_status' => 'pending',
        'post_author' => $current_user->ID,
        'post_category' => array($empresa_categoria_id)
    );



    $anuncio_id = wp_insert_post($argumentos);


    /* utilizamos las field_keys */
    update_field('field_5aa451a274744', $empresa_telefono, $anuncio_id);
    update_field('field_5aa451b874745', $empresa_direccion, $anuncio_id);
    update_field('field_5aa451cf74746', $empresa_sitio_web, $anuncio_id);
    update_field('field_5aa451e574747', $empresa_correo_electronio, $anuncio_id);
    update_field('field_5aa4517374743', $empresa_servicios, $anuncio_id);


    //HANDLE UPLOADED FILE
    if( !function_exists( 'wp_handle_sideload' ) ) {

      require_once( ABSPATH . 'wp-admin/includes/file.php' );

    }


    // Without that I'm getting a debug error!?
    if( !function_exists( 'wp_get_current_user' ) ) {

      require_once( ABSPATH . 'wp-includes/pluggable.php' );

    }

    $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LexAk4UAAAAABDb4OWt2bfEtVayCkap9NNPPTou&response={$response}");
    $captcha_success=json_decode($verify);


    if (!$captcha_success->success){ 
        
        $error = true;
        $msg = 'Recaptcha Inválido.';
        $referer = get_page_url('fichaempresaedicion');//(isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
        //var_dump('error: '.$referer); die();
        $referer = add_query_arg('status', $error, $referer);
        $referer = add_query_arg('msg', urlencode($msg), $referer);

        wp_redirect($referer);
        die();
    }

    if ($eliminar_logo == true){

        delete_post_thumbnail( $anuncio_id );            

    } else {
    
        if (!empty($_FILES['customLogo']['name'])){

            $tmpName = $_FILES['customLogo']['tmp_name'];
            $original_filename = $_FILES['customLogo']['name'];

            $updated_post = get_post($anuncio_id);

            $upload_dir   = wp_upload_dir();
            $upload_path = $upload_dir['path'].'/';
            $upload_url = $upload_dir['url'];

            $filename  = md5( $original_filename . microtime() ) . '_' . $original_filename;
            $file_guid = $upload_url.'/'.basename($filename);
            $file_path = $upload_path.'/'.$filename;

            if (copy($tmpName, $upload_path . $filename)){

                // Check the type of file. We'll use this as the 'post_mime_type'.
                $filetype = wp_check_filetype( basename( $file_path ), null );
                print_r($filetype);


                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid'           => $file_guid, 
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );

                // Insert the attachment.
                $attach_id = wp_insert_attachment( $attachment, $file_path, $anuncio_id );            

                // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
                require_once( ABSPATH . 'wp-admin/includes/image.php' );

                // Generate the metadata for the attachment, and update the database record.
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
                wp_update_attachment_metadata( $attach_id, $attach_data );

                set_post_thumbnail( $anuncio_id, $attach_id );            
            }
        }
    }

    /* Envio de correo de aviso */
    $correo_aviso = get_field('mail_aviso', 'option');
   // echo "correo_aviso: ".$correo_aviso;
    if (is_email($correo_aviso)){


        $anuncioLink = admin_url('post.php?post='.$anuncio_id).'&action=edit';


        $body = '';
        if ($nuevo_anuncio == true){
            $body = file_get_contents(TEMPLATEPATH . '/template-mail-nuevo-anuncio.html');
        } else {
            $body = file_get_contents(TEMPLATEPATH . '/template-mail-edicion-anuncio.html');
        }

        $body = str_replace('{$anuncioLink}', $anuncioLink, $body);
        $body = str_replace('{$empresa_nombre}', $empresa_nombre, $body);
        $body = str_replace('{$sitio_url}', home_url(), $body);

        $to = $correo_aviso;
        $subject = 'Aviso de cambio de anuncio.';
        $message = $body;
        $headers = array('Content-Type: text/html; charset=UTF-8');

        // $response = wp_mail($correo_aviso, 'Aviso de cambio de anuncio.', $contenido_email_aviso);  
        
        wp_mail( $to, $subject, $message, $headers );


    }

    wp_redirect(get_url_anuncio_usuario());
    die();
}

function get_current_category_id($anuncio_id){

    $categorias = get_the_category($anuncio_id);
    if (count($categorias) > 0){
        $aCat = current($categorias);
        return $aCat->term_id;
    }

    return false;

}

function get_mime_type_by_extension($ext){
    $mimeTypes = wp_get_mime_types();
    foreach($mimeTypes as $key => $mt){
        if (strpos($key, $ext) !== false){
            return $mt;
        }
    }
    return 'text/plain';
}
function excerpt($limit) {

      $excerpt = explode(' ', get_the_excerpt(), $limit);
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...</p>';
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
}
function excerptclean($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      }
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
}

function cutext($text,$limit) {
    $excerpt = explode(' ', $text, $limit);
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...</p>
        <p class="mas-info small"><a class="btn btn-link btn-sm text-danger" href="'.get_the_permalink().'">CONOCÉ MÁS</a></p>';
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = strip_tags(implode(" ",$content)).'...</p><p class="small mas-info"><a href="'.get_the_permalink().'"><b> MÁS INFORMACIÓN</b></a>';
      } else {
        $content = strip_tags(implode(" ",$content)).'...</p><p class="small mas-info"><a href="'.get_the_permalink().'"><b> MÁS INFORMACIÓN</b></a>';
      }
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content);
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
}



function ocultar_paginas_buscador( $query ) {
    if ( ! $query->is_admin && $query->is_search && $query->is_main_query() ) {
        $query->set( 'post_type', 'post' );
    }
    return $query;
}
add_filter('pre_get_posts','ocultar_paginas_buscador');


function enviar_notificacion_author($post_ID, $post = null, $update = null){
    
    if ( 'publish' == $post->post_status ){
        $aAuthor = get_user_by( 'ID', $post->post_author );
        $body = file_get_contents(TEMPLATEPATH . '/template-mail-publicar-anuncio.html');


        $body = str_replace('{$anuncioLink}', $anuncioLink, $body);
        $body = str_replace('{$empresa_nombre}', $post->post_title, $body);
        $body = str_replace('{$sitio_url}', home_url(), $body);

        $to = $aAuthor->user_email;;
        $subject = 'Hemos publicado tu anuncio!';
        $message = $body;
        $headers = array('Content-Type: text/html; charset=UTF-8');

        // $response = wp_mail($correo_aviso, 'Aviso de cambio de anuncio.', $contenido_email_aviso);  
        // $to = "nachoalbano@gmail.com";

        wp_mail( $to, $subject, $message, $headers );


    }
//    die();

}
add_action( 'save_post', 'enviar_notificacion_author', 10, 3 );