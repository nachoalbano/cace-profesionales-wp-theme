<div id="nav-principal" class="d-none d-lg-block">
  <div class="container-fluid cace-main-container">
    <nav class="navbar navbar-light bg-faded navbar-expand-sm">

      <div class="navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item home">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>"><i class="fa fa-home h4"></i></a>
          </li>
          <li class="nav-item medicos">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/cursos/medicos">Cursos Médicos</a>
          </li>
          <li class="nav-item estetica">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/cursos/estetica">Cursos de Estética</a>
          </li>
          <li class="nav-item online">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/cursos/online">Cursos Online</a>
          </li>
          <li class="nav-item cacetv">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/cacetv">Cace TV</a>
          </li>
          <li class="nav-item micace">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>">Mi Cace</a>
          </li>
          <li class="nav-item calendario">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/calendario">Calendario</a>
          </li>
          <li class="nav-item app">
            <a class="nav-link" href="<?php echo cace_global_url(); ?>/descarga-nuestra-app-micace">Descargar App</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</div>