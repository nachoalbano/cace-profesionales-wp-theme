                <nav id="offcanvas" class="navmenu navmenu-default navmenu-fixed-right offcanvas" role="navigation">
                    <ul class="list-unstyled offmenu">
                        <li><a href="#" class="text-right" data-toggle="offcanvas" data-target="#offcanvas" data-canvas="body"><i class="fa fa-times"></i></a></li>
                        <li class="button"><a target="_blank"  href="<?php echo cace_global_url(); ?>/micace"><strong>MICACE</strong></a></li>
                        <li class="button"><a target="_blank"  href="<?php echo cace_global_url(); ?>/ingresar">INGRESAR</a></li>
                        <li class="button"><a target="_blank"  href="<?php echo cace_global_url(); ?>/registro">REGISTRARSE</a></li>

                    </ul>

                    <ul class="list-unstyled offmenu full">
                        <li class="bg-medicos full"><a target="_blank"  href="<?php echo cace_global_url(); ?>/cursos/medicos" class="txt-white">CURSOS MÉDICOS</a></li>
                        <li class="bg-estetica full"><a target="_blank"  href="<?php echo cace_global_url(); ?>/cursos/estetica" class="txt-white">CURSOS DE ESTÉTICA</a></li>
                        <li class="bg-online full"><a target="_blank"  href="<?php echo cace_global_url(); ?>/cursos/online" class="txt-white">CURSOS ONLINE</a></li>
                        <li class="bg-darkgrey full"><a target="_blank"  href="<?php echo cace_global_url(); ?>/cacetv" class="txt-white">CACETV</a></li>
                        <li class="bg-darkgrey full"><a target="_blank"  href="<?php echo cace_global_url(); ?>/descarga-nuestra-app-micace" class="txt-white">DESCARGAR APP</a></li>
                        <li class="bg-darkgrey full"><a href="<?php echo home_url(); ?>" class="txt-white">PROFESIONALES</a></li>

                    </ul>
                    
                    <ul class="list-unstyled offmenu">
                        <li>
                            <a target="_blank"  href="<?php echo cace_global_url(); ?>/contacto"> CONTACTO</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#form-newsletter">NEWSLETTER</a>
                        </li>
                    </ul>
                    <ul class="list-unstyled offmenu social">
                        <li>
                            <a href="http://facebook.com/caceglobal"  target="_blank"><span style="padding: 2px 10px;"><i class="fa fa-facebook"></i></span></a>
                            <a href="http://twitter.com/caceglobal"  target="_blank"><span  style="padding: 2px 6px;"><i class="fa fa-twitter"></i></span></a>
                            <a href="http://youtube.com/caceglobal"  target="_blank"><span style="padding: 2px 7px;"><i class="fa fa-youtube"></i></span></a>
                            <a href="http://instagram.com/caceglobal" target="_blank"><span  style="padding: 2px 7px;"><i class="fa fa-instagram"></i></span></a>
                        </li>
                    </ul>
                </nav>
