<?php
/**
 * Estamos copiando la estructara del archivo header.php del theme de cace_3.1
 * Haremos algunos ajustes para que no sea dinámico, y que los enlaces te lleven a otra salapa 
 * target='_blank'
 */
?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?php wp_title( '|', true, 'right' ); ?>
        </title>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">            
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/cace-profesionales.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/cace-media-queries.css">
    </head>
    <div class="loading-content home d-none" id="top-loader">
        <span class="fa fa-circle-o-notch"></span>
    </div>
    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site">
            <div class="top-header">
                <div class="container-fluid cace-main-container text-right">
                    <ul class="list-inline">
                          <li class="list-inline-item mr-2">
                            <a href="<?php echo home_url(); ?>">GUÍA DE PROVEEDORES (nuevo)</a>
                          </li>
                        <li class="list-inline-item d-none d-sm-inline-block">
                            <i class="fa fa-globe" aria-hidden="true"></i> Seleccione región <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </li>
                        <li class="list-inline-item">
                            <a href="http://caribe.caceglobal.org">CARIBE|PANAMÁ <img src="<?php echo get_template_directory_uri();?>/assets/img/panama.png" width="15"></a> 
                        </li>
                        <li class="list-inline-item">
                            <span style="color: #efa11d;">ARGENTINA</span> <img src="<?php echo get_template_directory_uri();?>/assets/img/argentina.png" width="15">
                        </li>
                    </ul>
                </div>
            </div>
            <header id="masthead" class="site-header" role="banner">
                <div class="primary-header bg-blue">
                    <div class="container-fluid cace-main-container">
                        <div class="row">
                            <div class="col-lg-5">

                                <button class="btn btn-primary pull-right d-lg-none" id="small-button" data-toggle="offcanvas" data-target="#offcanvas" data-canvas="body">
                                    <i class="fa fa-bars"></i>
                                </button>

                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                                    <div class="logo"></div>
                                    <div class="site-description d-none d-md-block">
                                        <h1><img src="<?php echo get_template_directory_uri() ?>/assets/img/consejotxt.png" title="Consejo Argentino de Ciencias Estéticas" alt="Consejo Argentino de Ciencias Estéticas" /></h1>
                                    </div>

                                </a>
                            </div>
                            <div class="col-lg-7 text-right d-none d-lg-block" id="secondary-menu">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="<?php echo cace_global_url(); ?>/contacto">CONTACTO</a></li>
                                    <li class="list-inline-item"><a href="#" data-toggle="modal" data-target="#form-newsletter">NEWSLETTER</a></li>
                                    <li class="list-inline-item" style="font-size: 15px;"><i class="fa fa-phone"></i> +54 (011) 5778 7878</li>
                                    <li class="list-inline-item icon"><a href="http://facebook.com/caceglobal"><span style="padding: 2px 10px;"><i class="fa fa-facebook"></i></span></a></li>
                                    <li class="list-inline-item icon"><a href="http://twitter.com/caceglobal"><span  style="padding: 2px 6px;"><i class="fa fa-twitter"></i></span></a></li>
                                    <li class="list-inline-item icon"><a href="http://youtube.com/caceglobal"><span  style="padding: 2px 6px;"><i class="fa fa-youtube"></i></span></a></li>
                                    <li class="list-inline-item icon"><a href="http://instagram.com/caceglobal"><span  style="padding: 2px 7px;"><i class="fa fa-instagram"></i></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php get_template_part('header','main-menu'); ?>
            </header>
            <div class="container-fluid cace-main-container bg-celeste main-title">
                <div class="row">
                    <div class="col-12 col-md-6 text-center text-md-left">
                        <?php if (is_search()){ ?>
                        <h1>resultados para la busqueda <small><?php the_search_query(); ?></small></h1>
                        <?PHP } else if (is_home()){ ?>
                        <h1>GUÍA DE PROVEEDORES</h1>
                        <?PHP } else if (is_single()) { 
                            $categorias = get_the_category();
                            $arrCat = array();
                            foreach($categorias as $aCat){
                                $arrCat[] = $aCat->name;
                            }

                            $categoriasStr = implode(' - ', $arrCat);
                        ?>
                        <h1><?php echo $categoriasStr; ?></h1>
                        <?PHP } else { ?>
                        <h1>GUÍA DE PROVEEDORES</h1>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-md-6 text-center text-md-left">
                        <?php if (!is_user_logged_in()){ ?>
                            <a class="btn btn-link text-white float-md-right" href="<?php echo get_registro_url(); ?>"><i class="fa fa-address-card-o" aria-hidden="true"></i>
 REGISTRÁ TU EMPRESA</a>
                            <a class="btn btn-link text-white float-md-right" href="<?php echo get_ingreso_url(); ?>"><i class="fa fa-sign-in" aria-hidden="true"></i>
 INGRESAR</a>
                            <?php } else { ?>
                            <a class="btn btn-link text-white float-md-right" href="<?php echo get_url_anuncio_usuario(); ?>"><i class="fa fa-address-card-o" aria-hidden="true"></i>

 TU ANUNCIO</a>
                            <a class="btn btn-link text-white float-md-right" href="<?php echo wp_logout_url(home_url()); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>
 SALIR</a>
                        <?php } ?> 
                          <form method="get" class="searchform float-md-right mr-4" action="<?php bloginfo('url'); ?>/">
                            <input type="text" class="form-control" placeholder="Buscar" value="<?php the_search_query(); ?>" name="s" id="s" />
                            <i class="fa fa-search"></i>
                        </form>   
                    </div>
                </div>
           </div>


            <?php get_template_part('header','offcanvas-menu'); ?>
