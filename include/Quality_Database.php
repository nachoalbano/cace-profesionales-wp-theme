<?php

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

class Quality_Database {
	static  private $instancia = NULL;
	private $tbl_prefix;
	private $charset_collate = '';
	private $quality_db_version = '1.4';
	private $db_handler;

	private function __construct() {
		global $wpdb;
		$this->db_handler = $wpdb;
   		$this->tbl_prefix = $this->db_handler->prefix . "marea_"; 		
		
		if ( ! empty( $this->db_handler->charset ) ) {
		  $this->charset_collate = "DEFAULT CHARACTER SET {$this->db_handler->charset}";
		}
		
		if ( ! empty( $this->db_handler->collate ) ) {
		  $this->charset_collate .= " COLLATE {$this->db_handler->collate}";
		}
		
	}

	static public function getInstance() {
	   if (self::$instancia == NULL) {
		  self::$instancia = new Quality_Database();
	   }
	   return self::$instancia;
	}

	public function checkIntegrity(){
		
		$installed_version = get_option('quality_db_version');
		

		if ( floatval($installed_version) <= $this->quality_db_version ) {

			$table_name = $this->tbl_prefix.'abonados';

			// echo "installed_version: $installed_version $table_name";

			
			$sql = "CREATE TABLE $table_name (
			  id int(9) NOT NULL AUTO_INCREMENT,
			  nro_abonado int  NOT NULL,
			  nombre varchar(255) DEFAULT '' NOT NULL,
			  domicilio varchar(255) DEFAULT '' NOT NULL,
			  documento varchar(255) DEFAULT '' NOT NULL,
			  telefono varchar(255) DEFAULT '' NOT NULL,
			  UNIQUE KEY id (id)
			) $this->charset_collate;";
			

			dbDelta( $sql );

			update_option('quality_db_version', $this->quality_db_version);
			
		}
		
	}


	public function getLogin($usuario, $password){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'usuarios where usuario="'.$usuario.'" and password = "'.$password.'"';
		$UsuarioData = $this->db_handler->get_results( $sqlQuery );
		return (count($UsuarioData) > 0)?$UsuarioData[0]:false;
	}


	public function getUsuario($usuario_id){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'usuarios where id="'.$usuario_id.'"';
		$UsuarioData = $this->db_handler->get_results( $sqlQuery );
		return (count($UsuarioData) > 0)?$UsuarioData[0]:false;
	}

	public function saveAbonado($data){
		$cliente_id = isset($data['id'])?intval($data['id']):0;
		
		if (empty($cliente_id)){
			unset($data['id']);
			$this->db_handler->insert($this->tbl_prefix.'abonados', $data);	
			return $this->db_handler->insert_id;
		} else {
			$data['id'] = $cliente_id;
			$this->db_handler->replace($this->tbl_prefix.'abonados', $data);	
			return $cliente_id;
		}
	}

	public function deleteAbonados(){
		return $this->db_handler->query('delete from '.$this->tbl_prefix.'abonados');	
	}

	private function getFiltrosUsuarios($searchTxt){
		$filterArr = array(
			'nro_abonado', 'nombre', 'domicilio', 'documento', 'telefono'
		);
		$filtros = array();
		foreach ($filterArr as $aFilter){
			$filtros[] = "(".$aFilter . ' like "%'.$searchTxt.'%")';
		}
		$filter = '('.implode(' or ', $filtros).')';

		return $filter;
	}

	public function existeAbonado($numero, $dni){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'abonados where nro_abonado="'.$numero.'" and documento="'.$dni.'"';
		//var_dump($sqlQuery); die();
		$checkAbonado = $this->db_handler->get_results( $sqlQuery );
		return (count($checkAbonado) > 0)?$checkAbonado[0]:false;
	}

	public function direccionCorrecta($direccion, $nro_abonado){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'abonados where nro_abonado="'.$nro_abonado.'" and id="'.$direccion.'"';
		//var_dump($sqlQuery); die();
		$checkAbonado = $this->db_handler->get_results( $sqlQuery );
		return (count($checkAbonado) > 0)?$checkAbonado[0]:false;
	}

	public function existeMailAbonado($email){
		/*$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'abonados where nro_abonado="'.$numero.'" and documento="'.$dni.'"';
		//var_dump($sqlQuery); die();
		$checkAbonado = $this->db_handler->get_results( $sqlQuery );
		return (count($checkAbonado) > 0)?$checkAbonado[0]:false;*/
		global $wpdb;
		$user = get_user_by( 'email', $email );		
		/*echo '<pre>';
		print_r($user);
		echo "</pre>";
		die();*/
		return ($user)?$user:false;
	}

	public function getDireccionesAleatorias($nro_abonado, $cantidad){

		$sqlQuery = '(SELECT * FROM '.$this->tbl_prefix.'abonados'.' where nro_abonado="'.$nro_abonado.'") UNION (SELECT * FROM '.$this->tbl_prefix.'abonados'.' where nro_abonado<>"'.$nro_abonado.'" ORDER BY RAND() LIMIT '.$cantidad.') ';
		
		//var_dump($sqlQuery); die();
		$allData = $this->db_handler->get_results( $sqlQuery );

		return $allData;
	}

	public function getClientes($page, $searchTxt = ''){

		$perPage = 50;
		$offset = ($page - 1)* $perPage;

		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'abonados'." where ".$this->getFiltrosUsuarios($searchTxt)." LIMIT $offset, $perPage";

		//echo $sqlQuery;
		$allData = $this->db_handler->get_results( $sqlQuery );

		return $allData;
	}

	public function getClientesCount($searchTxt = ''){
		$sqlQuery =  'SELECT COUNT(*) FROM '.$this->tbl_prefix.'abonados'." where ".$this->getFiltrosUsuarios($searchTxt);		
		$allData = $this->db_handler->get_var( $sqlQuery );

		return $allData;

	}


	public function deleteCliente($id){
		return $this->db_handler->delete($this->tbl_prefix.'abonados', array('id'=>$id));	
	}
	
	public function getRssByName($name){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'rss where name="'.$name.'"';
		$RSSdata = $this->db_handler->get_results( $sqlQuery );
		return (count($RSSdata) > 0)?$RSSdata[0]:false;
	}

	public function getRssByUrl($url){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'rss where url="'.$url.'"';
		$RSSdata = $this->db_handler->get_results( $sqlQuery );
		return (count($RSSdata) > 0)?$RSSdata[0]:false;
	}
	
	public function getRssById($id){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'rss where id="'.$id.'"';
		$RSSdata = $this->db_handler->get_results( $sqlQuery );
		return (count($RSSdata) > 0)?$RSSdata[0]:false;
	}
	
	public function existsUrl($url){
		$sqlQuery = 'SELECT count(*) FROM '.$this->tbl_prefix.'rss where url="'.$url.'"';
		$checkUrl = $this->db_handler->get_var( $sqlQuery );
		return ($checkUrl > 0)?true:false;
	}

	public function getAllRss(){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'rss';
		$allData = $this->db_handler->get_results( $sqlQuery );
		return $allData;
	}
	
	public function saveRSS($data){
		$rss_id = isset($data['id'])?intval($data['id']):0;
		
		if (empty($rss_id)){
			$this->db_handler->insert($this->tbl_prefix.'rss', $data);	
			return $this->db_handler->insert_id;
		} else {
			$data['id'] = $rss_id;
			$this->db_handler->replace($this->tbl_prefix.'rss', $data);	
			return $rss_id;
		}
	}
	
	public function deleteRssById($id){
		return $this->db_handler->delete($this->tbl_prefix.'rss', array('id'=>$id));	
	}
	
	public function existsUrlJob($urlJob){
//		echo "urlJov: ".$urlJob; die();
		$args = array(
			'post_type' => 'mb_jobs',
			'meta_query' => array(
				array(
					'key' => '_jobs_link',
					'value' => trim($urlJob)
				)
			)			
		);
		$query = new WP_Query( $args );	
		return ($query->have_posts())?true:false;
	}
	
	
	
	
	public function getCustomUrlByUrl($url){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'customs_url where url="'.$url.'"';
		$CUdata = $this->db_handler->get_results( $sqlQuery );
		return (count($CUdata) > 0)?$CUdata[0]:false;
	}
	
	public function getCustomUrlById($id){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'customs_url where id="'.$id.'"';
		$RSSdata = $this->db_handler->get_results( $sqlQuery );
		return (count($RSSdata) > 0)?$RSSdata[0]:false;
	}
	

	public function getAllCustomsUrl(){
		$sqlQuery = 'SELECT * FROM '.$this->tbl_prefix.'customs_url';
		$allData = $this->db_handler->get_results( $sqlQuery );
		return $allData;
	}
	
	public function saveCustomUrl($data){
		$rss_id = isset($data['id'])?intval($data['id']):0;
		
		if (empty($rss_id)){
			$this->db_handler->insert($this->tbl_prefix.'customs_url', $data);	
			return $this->db_handler->insert_id;
		} else {
			$data['id'] = $rss_id;
			$this->db_handler->replace($this->tbl_prefix.'customs_url', $data);	
			return $rss_id;
		}
	}
	
	public function deleteCustomUrlById($id){
		return $this->db_handler->delete($this->tbl_prefix.'customs_url', array('id'=>$id));	
	}
	
	public function getLastId(){
		return $this->db_handler->insert_id;
	}
	
	public function getAllJobs($from = null, $to=null){
//		echo "urlJov: ".$urlJob; die();

		ini_set('memory_limit', -1);
		
		$args = array(
			'post_type' => 'mb_jobs',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'caller_get_posts'=> 1						
		);

		if (!(is_null($from) || is_null($to))){
			$args['date_query'] = 
				array(
					'after'    => array(
									'year'  => $from->format('Y'),
									'month' => $from->format('n'),
									'day'   => $from->format('j'),
								 ),
					'before'    => array(
									'year'  => $to->format('Y'),
									'month' => $to->format('n'),
									'day'   => $to->format('j'),
								 ),
					'inclusive' => true,
			 	);					
		}

		
		$query = new WP_Query( $args );	
		return $query->get_posts();
	}
	
}