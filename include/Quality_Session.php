<?php

add_action( 'init', 'quality_session_start', 1);
add_action( 'wp_logout', 'quality_session_end' );
add_action( 'wp_login', 'quality_session_end' );

function quality_session_start() {
    if( ! session_id() ) {
        session_start();
    }
}


function quality_session_end() {
    session_destroy();
}


class Quality_Session {
	
	static  private $instancia = NULL;

	private function __construct() {
		
	}

	static public function getInstance() {
	   if (self::$instancia == NULL) {
		  self::$instancia = new Quality_Session();
	   }
	   return self::$instancia;
	}

	public function login($aUsuario){
		$_SESSION['quality_login_user'] = $aUsuario->id;
		$_SESSION['quality_login_user_data'] = serialize($aUsuario);
	}

	public function logout(){
		session_unset('quality_login_user');
		session_unset('quality_login_user_data');
		session_destroy();
	}

	public function getLogin(){
		
		if (isset($_SESSION['quality_login_user'])){
			$dataUsuario_id = intval($_SESSION['quality_login_user']);
			$dataUsuario = unserialize($_SESSION['quality_login_user_data']);

			return $dataUsuario;
		}

		return false;
	}
}

