<?php
/**
 * Register a custom menu page.
 */
function wpdocs_register_carne_menu_page() {
    
    add_menu_page( 
        'Abonados',
        'Abonados',
        'abonados-option',
        'clientes-options-listado',
        'carne_page_clientes',
        'dashicons-universal-access-alt',
        6
    ); 

    add_submenu_page( 
            'clientes-options-listado', 
            'Listado de abonados', 
            'Listado de abonados',
            'abonados-option',
            'clientes-options-listado',
            'carne_page_clientes'
    );


    add_submenu_page( 
            'clientes-options-listado', 
            'Nuevo Excel Abonados', 
            'Nuevo Excel Abonados',
            'abonados-option',
            'clientes-options-form',
            'carne_page_clientes_form_nuevo'

    );

}
add_action( 'admin_menu', 'wpdocs_register_carne_menu_page' );

/**
 * Display a custom menu page
 */
function carne_page_clientes(){
	include( WPHOST_DIR . '/administracion/clientes-listado.php');
}

function carne_page_clientes_form_nuevo(){
    include( WPHOST_DIR . '/administracion/clientes-form.php');    
}