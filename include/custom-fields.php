<?php 
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_empresa',
		'title' => 'Empresa',
		'fields' => array (
			array (
				'key' => 'field_5aa4517374743',
				'label' => 'Servicios',
				'name' => 'servicios',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5aa451a274744',
				'label' => 'Teléfono',
				'name' => 'telefono',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5aa451b874745',
				'label' => 'Dirección',
				'name' => 'direccion',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5aa451cf74746',
				'label' => 'Sitio web',
				'name' => 'sitio_web',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5aa451e574747',
				'label' => 'Correo electrónico',
				'name' => 'correo_electronico',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_informacion-complementaria-bienvenida',
		'title' => 'Información Complementaria Bienvenida',
		'fields' => array (
			array (
				'key' => 'field_5aa956c0077f5',
				'label' => 'Información Complementaria',
				'name' => 'informacion_complementaria',
				'type' => 'wysiwyg',
				'instructions' => 'Ingrese la información complementaria para mostrar en la parte izquierda de la pantalla.',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-bienvenida.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_options',
		'title' => 'Options',
		'fields' => array (
			array (
				'key' => 'field_5aa6b05d54756',
				'label' => 'Sidebar en publicacion',
				'name' => 'sidebar_en_publicacion',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_5aa6b07e54757',
				'label' => 'Correo electrónico para informar cambios',
				'name' => 'mail_aviso',
				'type' => 'email',
				'instructions' => 'Ingrese aquí el correo electrónico al cual se quiere dar aviso cuando un profesional ingresa o genera un cambio en su anuncio.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
