<?php 
/*
		Crea las paginas necesarias de wordpress para utilizar el template.
		las páginas que vamos a crear son
		Página de Profesionales - home
		La 'pagina de entradas' se define como el index.php que es exatamente igual al template de anuncios.
*/

class CacePageManager{

	protected $cacePagesUpdatedOption = 'OPT_CACE_PAGES_UPDATED';
	protected $arePagesUpdates = false;

	public function __construct(){
		// update_option($this->cacePagesUpdatedOption, false);
		$this->arePagesUpdates = get_option($this->cacePagesUpdatedOption);
		add_action('init', array($this, 'check_integrity'));
	}

	public function the_slug_exists($post_name) {
		global $wpdb, $table_prefix;

		$theQuery = "SELECT post_name FROM ".$table_prefix."posts WHERE post_name = '" . $post_name . "'";

		if($wpdb->get_row($theQuery, 'ARRAY_A')) {
			return true;
		} else {
			return false;
		}
	} 

	public function check_integrity(){

		if (!$this->arePagesUpdates){
		
			$pagesToCheck = array();

			$homePage = array(
				'title' => 'Página de Inicio.',
				'slug' => 'home',
				'content' => '',
				'template' => 'template-listado-anuncios.php'
			);
			$pagesToCheck[] = $homePage;


			$homeBienvenidaProfesionales = array(
				'title' => 'Página de Bienvenida.',
				'slug' => 'bienvenida',
				'content' => '',
				'template' => 'template-bienvenida.php'
			);
			$pagesToCheck[] = $homeBienvenidaProfesionales;

			$fichaEmpresa = array(
				'title' => 'Ficha de empresa.',
				'slug' => 'fichaempresa',
				'content' => '',
				'template' => 'template-ficha-empresa.php'
			);
			$pagesToCheck[] = $fichaEmpresa;

			$fichaEmpresaEdicion = array(
				'title' => 'Ficha de empresa (edicion).',
				'slug' => 'fichaempresaedicion',
				'content' => '',
				'template' => 'template-formulario-edicion.php'
			);
			$pagesToCheck[] = $fichaEmpresaEdicion;

			$paginaRegistroIngreso = array(
				'title' => 'Registro - Ingreso.',
				'slug' => 'ingreso',
				'content' => '',
				'template' => 'template-inicio-registro.php'
			);
			$pagesToCheck[] = $paginaRegistroIngreso;

			foreach ($pagesToCheck as $aPage){
				$aPage = (object) $aPage;

				
				if (!$this->the_slug_exists($aPage->slug)){

					$blog_page_id = wp_insert_post(array(
						'post_type' => 'page',
						'post_title' => $aPage->title,
						'post_content' => $aPage->content,
						'post_status' => 'publish',
						'post_author' => 1,
						'post_name' => $aPage->slug,
						'page_template'=> $aPage->template						
					));

					
				}
				
			}

			update_option($this->cacePagesUpdatedOption, true);
		}
	}

}

function get_registro_url(){
	return get_page_url('ingreso');
}

function get_ingreso_url(){
	return get_page_url('ingreso');
}

function get_bienvenida_url(){
	return get_page_url('bienvenida');
}

function get_ficha_url(){
	return get_page_url('fichaempresa');
}

function get_fichaedicion_url(){
	return get_page_url('fichaempresaedicion');
}


$wpCacePageManager = new CacePageManager();
