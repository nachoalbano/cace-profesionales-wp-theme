<?php 


/* Registrar un usuario */
add_action( 'admin_post_cace_registro', 'cace_registro' );
add_action( 'admin_post_nopriv_cace_registro', 'cace_registro' );
function cace_registro() {

    $nombre = $_POST['nombre'];
    $correo = $_POST['email'];
    $password = $_POST['password'];
    $password_repetir = $_POST['password-repetir'];
    $response = $_POST["g-recaptcha-response"];

    $error = false;

    $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LexAk4UAAAAABDb4OWt2bfEtVayCkap9NNPPTou&response={$response}");
    $captcha_success=json_decode($verify);

    if ($captcha_success->success==true){        

        if ($password != $password_repetir){
            $error = true;
            $msg = 'Las contraseñas no coinciden.';
        } else {

            $user_id = username_exists( $correo );
            if ( !$user_id and email_exists($correo) == false ) {
                $user_data = array(
                    'user_pass' => $password,
                    'user_login' => $correo,
                    'user_nicename' => $nombre,
                    'user_email' => $correo,
                    'role' => 'author'
                );

                $user_id = wp_insert_user( $user_data );

                if (is_wp_error( $user_id )){
                    $error = true;
                    $msg = $user_id->get_error_message();
                } else {

                    add_user_meta( $user_id, 'activo', 'NO' );
                    post_admin_envio_mail_validacion($user_id);

                }

            } else {
                $error = true;
                $msg = 'Ya existe un usuario';
            }        
        }
        
    } else {
        $error = true;
        $msg = 'Recaptcha Inválido.';
    }

    if (!$error){
        $msg = 'Te hemos registrado con éxito. Revisa tu correo electrónico para continuar';
    } 

    $referer = get_page_url('ingreso');//(isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : $_SERVER['PHP_SELF'];
    $referer = add_query_arg('status', $error, $referer);
    $referer = add_query_arg('msg', urlencode($msg), $referer);

    wp_redirect($referer);
}


add_action( 'admin_post_nopriv_envio_mail_validacion', 'post_admin_envio_mail_validacion' );
add_action( 'admin_post_envio_mail_validacion', 'post_admin_envio_mail_validacion' );
function post_admin_envio_mail_validacion ( $userID ) {

    global $wpdb;    

    if($userID == ''){        
        $userID = trim($_POST['userID']); 
    }

    $userinfo = get_user_by( 'ID', $userID );
    
    if ( $userinfo ) {
       
       $link_validacion = admin_url('admin-post.php');
       $link_validacion = add_query_arg('action', 'validar_usuario', $link_validacion);
       $link_validacion = add_query_arg('userId', $userinfo->ID, $link_validacion);


       $body = file_get_contents(TEMPLATEPATH . '/template-mail-validacion.html');
       $body = str_replace('{$verificar_link}', $link_validacion, $body);
       $body = str_replace('{$usuario}', $userinfo->data->display_name, $body);

       $to = $userinfo->data->user_email;
       $subject = 'Valide su usuario de Cace Profesionales';
       $message = $body;
       $headers = array('Content-Type: text/html; charset=UTF-8');

       wp_mail( $to, $subject, $message, $headers );
            
    }    
    
    if($_POST['userID'] != ''){
        get_template_part('template-envio-correo');
    }

}

add_action( 'admin_post_nopriv_validar_usuario', 'cace_validar_usuario' );
add_action( 'admin_post_validar_usuario', 'cace_validar_usuario' );
function cace_validar_usuario ( ) {

    global $wpdb;    
    $userId = $_GET['userId'];
    $userinfo = get_user_by( 'ID', $userId );

    if ( $userinfo ){
        update_user_meta( $userId, 'activo', 'SI' );
    }else{
        wp_redirect(home_url());
        exit;
    }

    get_header();
    get_template_part('template','validacion');
    get_footer();
    die();
}
