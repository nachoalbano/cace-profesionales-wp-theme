<?php // controlador de web service para obtener los datos de la administración

	class Quality_WS {
		private $__urlServicioSoap;
		private $__SoapClient;

		private $__authentication_user;
		private $__authentication_password;

		private $__identificacion;
	
		static  private $instancia = NULL;


		public function __construct(){
			$this->__urlServicioSoap = 'http://qssoft.no-ip.org:8080/WebSiteSegSoap/Service.asmx?WSDL';
			$this->__authentication_user = 'LEMUS';
			$this->__authentication_password = 'LEMUS';
			$this->__identificacion = $this->Autenticacion();
		}

		private function getClient(){
			$aSoapClient = new SoapClient($this->__urlServicioSoap);
			return $aSoapClient;
		}

		static public function getInstance() {
		   if (self::$instancia == NULL) {
			  self::$instancia = new Quality_WS();
		   }
		   return self::$instancia;
		}

		private function Autenticacion(){
			$parametros = array(
				'Usuario' => $this->__authentication_user,
				'Password' => $this->__authentication_password,
			);
			$result = $this->getClient()->__SoapCall('Autenticacion', array($parametros));
			$resultadoXML = $result->AutenticacionResult;								// transformo el resultado a XML
			return $resultadoXML;
		}

		private function parse_result($resultadoXML){
			$dataset = simplexml_load_string($resultadoXML);
			$data = array();

			if (property_exists($dataset, 'NewDataSet')){
				if (property_exists($dataset->NewDataSet, 'Table')){
					$data = $dataset->NewDataSet->Table;
				}
			} 
			// print_r($data); die();
			return $data;
		}


		public function getDatos($usuario, $fechaDesde = null, $fechaHasta = null){

			$fechaActual = new \Datetime('now');

			if (is_null($fechaHasta)){
				$fechaHasta = new \Datetime('now');
			}

			if (is_null($fechaDesde)){
				$fechaDesde = clone $fechaActual;
				$fechaDesde->modify('-365 day');
			}

			$parametros = array(
				'identificacion' => $this->__identificacion,
				'Usuario' => $usuario,
				'fechaIni' => $fechaDesde->format('Y-m-d'),
				'fechaFin' => $fechaHasta->format('Y-m-d')
			);

		
			try{

				$resultado = $this->getClient()->__SoapCall('GetDatos', array($parametros));
				$resultadoXML = $resultado->GetDatosResult;					// transformo el resultado a XML
			} catch (\Exception $e){
				post_admin_do_logout();
				echo "<p>".$e->getMessage()."</p><pre>";
				var_dump($e);
				die();
			}

			return $this->parse_result($resultadoXML->any);
		}

		public function getDatosPropietario($usuario){


			$parametros = array(
				'identificacion' => $this->__identificacion,
				'Usuario' => $usuario
			);

		
			try{

				$resultado = $this->getClient()->__SoapCall('GetDatosPropietario', array($parametros));
				$resultadoXML = $resultado->GetDatosPropietarioResult;					// transformo el resultado a XML

			} catch (\Exception $e){
				echo "<p>".$e->getMessage()."</p><pre>";
				var_dump($e);
				die();
			}

			return $this->parse_result($resultadoXML->any);

		}


		public function getDatosCliente($usuario){


			$parametros = array(
				'identificacion' => $this->__identificacion,
				'Usuario' => $usuario
			);

		
			try{

				$resultado = $this->getClient()->__SoapCall('GetDatosCLIENTE', array($parametros));
				$resultadoXML = $resultado->GetDatosCLIENTEResult;					// transformo el resultado a XML

			} catch (\Exception $e){
				echo "<p>".$e->getMessage()."</p><pre>";
				var_dump($e);
				die();
			}

			return $this->parse_result($resultadoXML->any);

		}


		public function getArchivos($usuario, $fechaDesde = null, $fechaHasta = null){

			$fechaActual = new \Datetime('now');

			if (is_null($fechaHasta)){
				$fechaHasta = new \Datetime('now');
			}

			if (is_null($fechaDesde)){
				$fechaDesde = clone $fechaActual;
				$fechaDesde->modify('-365 day');
			}

			$parametros = array(
				'identificacion' => $this->__identificacion,
				'Usuario' => $usuario,
				'fechaIni' => $fechaDesde->format('Y-m-d'),
				'fechaFin' => $fechaHasta->format('Y-m-d')
			);

		
			try{

				$resultado = $this->getClient()->__SoapCall('GetListaArchivos', array($parametros));
				$resultadoXML = $resultado->GetListaArchivosResult;					// transformo el resultado a XML
			} catch (\Exception $e){
				post_admin_do_logout();
				echo "<p>".$e->getMessage()."</p><pre>";
				var_dump($e);
				die();
			}

			return $this->parse_result($resultadoXML->any);
		}

	}


