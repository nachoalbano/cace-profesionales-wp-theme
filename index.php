<?php
	get_header();

	$categoriasAll = get_categories();

?>


<div class="container-fluid cace-main-container container-profesionales mb-5">
	<div class="row">
		<div class="sidebar order-12 order-md-1 col-md-3 col-lg-3 col-xl-2">
			<div class="sidebar-inner">
				<div class="wpr-categorias">
					<h3>Categorías</h3>
					<ul class="list-unstyled list-categorias">
					<?php foreach ($categoriasAll as $aCat) { ?>
						<li><a class="btn-categoria" href="<?php echo get_category_link($aCat->term_id); ?>"><?php echo $aCat->name; ?></a></li>
					<?php } ?>
					</ul>
				</div>
			</div>
		</div>

		<div class="col order-1 order-md-12">

			<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post(); 
					//
					// Post Content here
					//
	
					echo get_template_part('listado', 'anuncio');				

				} // end while
			}else{ ?>
				<h2 class="mt-4 pt-4">No encontramos nada parecido a lo que buscas. Intentá nuevamente.</h2>
			<?php } // end if
			?>			
			<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="5" offset="5" progress_bar="true" progress_bar_color="483A7A" images_loaded="true"]'); ?>
		</div>

	</div>
</div>

<?php get_footer();