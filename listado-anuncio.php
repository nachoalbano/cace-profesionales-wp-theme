<?php
					$linkEmpresa = get_permalink();
                    $categorias = get_the_category();
                    $arrCat = array();
                    foreach($categorias as $aCat){
                    	//print_r($aCat);
                        $arrCat[] = '<a href="'.get_category_link( $aCat->term_id ).'" title="'.$aCat->name.'">'.$aCat->name.'</a>';
                    }
                    $categoriasStr = implode(' - ', $arrCat);
                    $telefono = get_field('telefono', get_the_ID());
                    $direccion = get_field('direccion', get_the_ID());
                    $sitio_web = get_field('sitio_web', get_the_ID());
                    $correo_electronico = get_field('correo_electronico', get_the_ID());

                    ?>
					<article class="profesional">
						<div class="row">
							<div class="col-md-12 col-xl-5">
								<div class="row">
									<div class="col-md-5 mb-3 mb-md-0">
										<a href="<?php echo $linkEmpresa; ?>">
											<?php the_post_thumbnail( 'medium', array('class' => 'img-fluid w-100')); ?>
											<!--<img src="<?php echo get_thumb(get_the_ID()); ?>" align="" alt="<?php the_title(); ?>" class="img-fluid w-100" />-->
										</a>
									</div>
									<div class="col-md-7">
										<h3><?php echo $categoriasStr; ?></h3>
										<h2><a href="<?php echo $linkEmpresa; ?>"><?php the_title(); ?></a></h2>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-lg-3 mt-3 mt-md-4 mt-xl-0">
								<ul class="list-unstyled fa-ul contacto">
								  <?php echo (!empty($telefono))?'<li><i class="fa-li fa fa-phone"></i>'.$telefono.'</li>':''; ?>
								  <?php echo (!empty($direccion))?'<li><i class="fa-li fa fa-home"></i>'.$direccion.'</li>':''; ?>
								  <?php echo (!empty($sitio_web))?'<li><a href="'.$sitio_web.'" target="_blank"><i class="fa-li fa fa-globe"></i>'.$sitio_web.'</a></li>':''; ?>
								  <?php echo (!empty($correo_electronico))?'<li><a href="mailto:'.$correo_electronico.'" target="_blank"><i class="fa-li fa fa-envelope-o"></i>'.$correo_electronico.'</a></li>':''; ?>
								</ul>
							</div>
							<div class="col-md-8 mt-0 mt-md-3 mt-xl-0 col-xl-4">
								<div class="descripcion">
									<?php echo excerpt(30) ?>
									<a href="<?php echo $linkEmpresa; ?>" class="btn-mas">Ver más <i class="fa fa-angle-down"></i></a>
								</div>
							</div>
						</div>
					</article>
