<?php
	get_header();
	 $categorias = get_the_category();
	    $categoriaPrincipal = $categorias[0];

	    $post_de_la_categoria = get_posts(
	    	array(
	    		'category' => $categoriaPrincipal->term_id,
	    		'exclude' => get_the_ID() 
	    	)
	    );
?>


<div class="container-fluid cace-main-container container-profesionales">
	<div class="row ">
		<div class="sidebar order-12 order-md-1 col-md-3 col-lg-3 col-xl-2">
			<div class="sidebar-inner">
				<?php if (!$is_current_user_post) {?>
				<div class="wpr-categorias">
					<h3> OTROS ANUNCIOS DE <?php echo $categoriaPrincipal->name; ?></h3>
					<ul class="list-unstyled list-categorias">
					<?php foreach ($post_de_la_categoria as $aPost) { 

						$aPost_site_url = get_field('sitio_web', $aPost->ID);
						
						?>
						<li>
							<a href="<?php echo get_permalink($aPost->ID); ?>"><?php echo $aPost->post_title; ?></a><br />
							<?php if (!empty($aPost_site_url)) { echo '<a target="_blank" href="#"><i class="fa fa-globe"></i> '.$aPost_site_url.'</a>'; } ?>
						</li>
					<?php } ?>
					</ul>
					<!--<a class="btn btn-block btn-seconday btn-gris mb-4" href="<?php echo get_category_link($categoriaPrincipal->term_id); ?>">ver más</a>-->
				</div>
				<?php } else { ?>
				<div class="option-sidebar">
					<?php the_field('sidebar_en_publicacion', 'option'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="col order-1 order-md-12">

			<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post(); 
					//
					// Post Content here
					//

					$currentStatus = get_post_status();
					
					$linkEmpresa = get_permalink();

                   

                    $telefono = get_field('telefono');
                    $direccion = get_field('direccion');
                    $sitio_web = get_field('sitio_web');
                    $correo_electronico = get_field('correo_electronico');

                    $servicios = get_field('servicios');

                    $author_id = get_the_author_meta( 'ID' );
                    $current_user = wp_get_current_user();

                    $is_current_user_post = (is_user_logged_in() && ($current_user->ID == $author_id))?true:false;
			?>
					<article class="ficha profesional">
						<?php if ($is_current_user_post) { ?>
						<div class="row ficha-header mb-4">
							<div class="col">
								<h2 class="text-uppercase">publicación actual</h2>
							</div>
							<div class="col-md-6 text-md-right">
								<?php if ($currentStatus != 'publish') { ?>
								<span class="text-info mr-3"> <i class="fa fa-clock-o" aria-hidden="true"></i>
En espera de aprobación </span>
								<?php } else { ?>
								<span class="text-success mr-3"><i class="fa fa-check" aria-hidden="true"></i>
 PUBLICADA </span>
								<?php } ?>
								<a href="<?php echo get_edit_url(); ?>" class="btn btn-seconday btn-gris"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
 EDITAR</a>
							</div>
						</div>	
						<?php } ?>
						<div class="row">
							<div class="col-md-4">
								<div class="mb-3">
									<img src="<?php echo get_thumb(get_the_ID(), 'ficha'); ?>" align="" alt="Empresa nombre" class="mr-4 img-fluid" />
								</div>
								<h2 class="mb-4"><?php the_title(); ?></h1>

								<ul class="list-unstyled fa-ul contacto">
								  <?php echo (!empty($telefono))?'<li><i class="fa-li fa fa-phone"></i>'.$telefono.'</li>':''; ?>
								  <?php echo (!empty($direccion))?'<li><i class="fa-li fa fa-home"></i>'.$direccion.'</li>':''; ?>
								   <?php echo (!empty($sitio_web))?'<li><a href="'.$sitio_web.'" target="_blank"><i class="fa-li fa fa-globe"></i>'.$sitio_web.'</a></li>':''; ?>
								  <?php echo (!empty($correo_electronico))?'<li><a href="mailto:'.$correo_electronico.'" target="_blank"><i class="fa-li fa fa-envelope-o"></i>'.$correo_electronico.'</a></li>':''; ?>
								</ul>

							</div>
							<div class="<?php echo (empty($servicios)?'col-md-8':'col-md-4'); ?>">
								<h3>La Empresa</h3>
								<div class="descripcion">
									<?php the_content(); ?>
								</div>
							</div>
							<?php if (!empty($servicios)) { ?>
							<div class="col-md-4">
								<h3>Servicios</h3>
								<div class="descripcion">
									<?php echo $servicios; ?>
								</div>
							</div>
							<?php } ?>
						</div>
					</article>
			<?php
				} // end while
			} // end if
			?>			
		</div>
		
	</div>
</div>

<?php get_footer();