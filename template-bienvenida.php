<?php
/*
Template Name: Página de bienvenida
*/

	if (!is_user_logged_in()){
		wp_redirect(home_url());
		die();
	}


	get_header();
	the_post();
?>
<div class="container-fluid cace-main-container container-profesionales formulario mt-5">
	<div class="row">

		<div class="col-md-4">
			<div class="wpr-bienvenida">
				<h2><?php the_title(); ?></h2>
				<div class="descripcion">
					<?php the_content();  ?>
				</div>
				<a href="<?php echo get_edit_url(); ?>" class="btn btn-celeste btn-secondary"> crear tu anuncio </a>
			</div>
		</div>

		<div class="col">
			<?php 
				the_field('informacion_complementaria');
			?>			

		</div>
	</div>
</div>

<?php get_footer();