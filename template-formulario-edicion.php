<?php
/*
	Template Name: Formulario de anuncio
*/

	if (!is_user_logged_in()){
		wp_redirect(home_url());
		die();
	}

	$current_post_id = get_anuncio_id();
	$current_post = get_post($current_post_id);

	$empresa_nombre = ($current_post_id)?get_the_title($current_post_id):'';
	$empresa_telefono = ($current_post_id)?get_field('telefono',$current_post_id):'';
	$empresa_direccion = ($current_post_id)?get_field('direccion',$current_post_id):'';
	$empresa_sitio_web = ($current_post_id)?get_field('sitio_web',$current_post_id):'';
	$empresa_correo_electronico = ($current_post_id)?get_field('correo_electronico',$current_post_id):'';
	$empresa_contenido = ($current_post_id)?$current_post->post_content:'';
	$empresa_servicios = ($current_post_id)?get_field('servicios',$current_post_id):'';
	$empresa_imagen_id = get_thumb_id($current_post_id);
	$empresa_imagen_url = get_thumb($current_post_id);

	$empresa_categoria_id = get_current_category_id($current_post_id);


	$categoriasAll = get_categories(array(
		'hide_empty' => false
	));

	$registroFail = isset($_GET['status'])?intval($_GET['status']):0;
	$registroFail = ($registroFail == 1)?true:false;
	$registroMessage = strip_tags(trim($_GET['msg']));

	get_header();
?>
<div class="container-fluid cace-main-container container-profesionales formulario mt-5">
	<div class="row">

		<div class="col-md-4">
			<form class="form frmEdicion" action="<?php echo admin_url('admin-post.php'); ?>" method='post' enctype="multipart/form-data">
				<input type="hidden" name="action" value="guardar_anuncio">
				<h2>Agregar una publicación</h2>
				<input type="hidden" name="current_post_id" value="<?php echo $current_post_id; ?>">
				<div class="form-group">

					<div class="input-group">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text"><i class="fa fa-user"></i></span>
					  	</div>
						<input type="text" value="<?php echo $empresa_nombre; ?>" class="form-control empresa-nombre" name="nombre" placeholder="Nombre de la empresa">
					</div>

				</div>
				<div class="form-group">
					<input type="hidden" name="imagen_id" value="<?php echo $empresa_imagen_id; ?>" />
					<div style="overflow: hidden; height: 0px;"><input type="file" class="custom-file-input" name="customLogo" id="customLogo"></div>
				    <div class="errorLogo"></div>
						<img id="imgLogoPreview" width="110" src="<?php echo $empresa_imagen_url; ?>" align="" alt="<?php echo $empresa_nombre; ?>" class="mb-2 mr-2 float-left" />
						<p class="message mb-0 <?php echo (!intval($empresa_imagen_id))?'d-none':'';?>" id="msgActual">Logo actual.</p>
						<p class="message text-success mb-0 mt-4 d-none" id="msgSeleccionada"><i class="fa fa-check"></i> Esta es la nueva imagen seleccionada.</p>
						<p><a href='#' id="btnEliminarLogo" class="btn-eliminar-logo text-danger">Eliminar</a></p>
						<button type="button" class="btn-handle-logo btn btn-block btn-secondary btn-gris"><?php echo ($empresa_imagen_id)?'Subir otro archivo':'Carga tu logo'; ?></button>
						<input type="hidden" name="inputLogoEliminar" id="inputLogoEliminar" value="">
				</div>
				<div class="form-group">
					<select class="form-control" name="categoria_id" placeholder='Seleccione una categoría' required>
						<option value="">Seleccione una categoría</option>
						<?php foreach($categoriasAll as $aCat){ ?>
						<option <?php echo ($aCat->term_id == $empresa_categoria_id)?'selected':''; ?> value="<?php echo $aCat->term_id; ?>"><?php echo $aCat->name; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text"><i class="fa fa-phone"></i></span>
					  	</div>
						<input type="text" value="<?php echo $empresa_telefono; ?>" class="form-control empresa-telefono" name="telefono" placeholder="Teléfono">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text"><i class="fa fa-home"></i></span>
					  	</div>
						<input type="text" value="<?php echo $empresa_direccion; ?>" class="form-control empresa-direccion" name="direccion" placeholder="Dirección">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text"><i class="fa fa-globe"></i></span>
					  	</div>
						<input type="text" value="<?php echo $empresa_sitio_web; ?>" class="form-control empresa-web" name="sitio_web" placeholder="Sitio web">
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
					  	<div class="input-group-prepend">
					    	<span class="input-group-text"><i class="fa fa-envelope-o"></i></span>
					  	</div>
						<input type="text" value="<?php echo $empresa_correo_electronico; ?>" class="form-control empresa-correo" name="correo_electronico" placeholder="Correo electrónico">
					</div>
				</div>
				<div class="form-group">
					<textarea class="form-control" placeholder="Información sobre tu empresa" name="content"><?php echo $empresa_contenido; ?></textarea>
				</div>
				<div class="form-group">
					<textarea class="form-control" placeholder="Información sobre los servicios" name="servicios"><?php echo $empresa_servicios; ?></textarea>
				</div>

				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="6LexAk4UAAAAADPW71gtIr4DeU72-Xb7pyY8-1Di"></div>
				</div>
				<?php if (!empty($registroMessage)) { ?>
					<div class="alert my-2 alert-<?php echo ($registroFail)?'danger':'success'; ?>">
						<?php echo $registroMessage; ?>
					</div>
					<?php } ?>
				<div class="form-group mt-4">
					<button type="submit" class="btn btn-default btn-celeste btn-block">enviar</button>
				</div>
			</form>
			
		</div>

		<div class="col">
			<?php 
			if ( have_posts() ) {
				while ( have_posts() ) {
					the_post(); 
					the_content();
				} // end while
			} // end if
			?>			

		</div>
	</div>
</div>

<?php get_footer();