<?php
/*
Template Name: Página de inicio y registro  
*/

	if (is_user_logged_in()){
		wp_redirect(home_url());
		die();
	}

	get_header();

	$resultFail = isset($_GET['result'])?trim($_GET['result']):false;
	$username = isset($_GET['username'])?trim($_GET['username']):'';

	$registroFail = isset($_GET['status'])?intval($_GET['status']):0;
	$registroFail = ($registroFail == 1)?true:false;
	$registroMessage = strip_tags(trim($_GET['msg']));

?>
<div class="container-fluid cace-main-container">
	<div class="wpr-registro-inicio">
		<div class="row">
			<div class="col col-md-6 col-12">
				
				<form class="form frmIngreso" action="<?php echo wp_login_url(home_url()); ?>" method='post'>
					
					<h2>INGRESAR A TU CUENTA</h2>	
					
					<div class="form-group">
						<input type="text" class="form-control" id="user_login" name="log" placeholder="Tu email" value="<?php echo $username; ?>">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="user_password" name="pwd" placeholder="Contraseña">
					</div>
					<div class="form-group">
						<label for="rememberme"><input name="rememberme" id="rememberme" value="forever" type="checkbox"> Recordarme</label>
					</div>
					<?php if ($resultFail != false) { ?>
					<div class="alert alert-danger my-4">
						Los datos son incorrectos. Intente nuevamente.
					</div>
					<?php } ?>
					<div class="form-group mt-4">
						<button name="wp-submit" type="submit" class="btn btn-default btn-celeste btn-block">ingresar</button>
					</div>
					<a href="#" class="btn btn-block btn-link text-gray-dark">¿No recordás tu contraseña?</a>
					<input type="hidden" name="recirect_to" value="<?php home_url(); ?>">
				</form>
			</div>
			<div class="col-md-6 col-12">	
				

				<form class="form frmRegistro" action="<?php echo admin_url('admin-post.php'); ?>" method='post'>
					<input type="hidden" name="action" value="cace_registro">
					<h2>REGISTRATE COMO PROVEEDOR</h2>

					<div class="form-group">
						<input type="text" class="form-control" name="nombre" placeholder="Nombre" >
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="email" placeholder="Tu email" >
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" >
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password-repetir" placeholder="Repite Contraseña" >
					</div>
					<div class="form-group">
						<div class="g-recaptcha" data-sitekey="6LexAk4UAAAAADPW71gtIr4DeU72-Xb7pyY8-1Di"></div>
					</div>
					<?php if (!empty($registroMessage)) { ?>
					<div class="alert my-2 alert-<?php echo ($registroFail)?'danger':'success'; ?>">
						<?php echo $registroMessage; ?>
					</div>
					<?php } ?>
					<div class="form-group mt-4">
						<button type="submit" class="btn btn-default btn-celeste btn-block">registrarse</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php get_footer();