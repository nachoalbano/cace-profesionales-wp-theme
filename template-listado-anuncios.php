<?php
/*
Template Name: Página de Anuncios 
*/
	get_header();
?>
<div class="container-fluid cace-main-container bg-celeste main-title">
	<div class="row">
		<div class="col">
			<h1>anuncios de profesionales</h1>
		</div>
		<div class="col-md-3">
			<form method="get" class="searchform" action="<?php bloginfo('url'); ?>/">
				<input type="text" class="form-control" placeholder="Buscar" value="<?php the_search_query(); ?>" name="s" id="s" />
				<i class="fa fa-search"></i>
			</form>		
		</div>
	</div>
</div>


<div class="container-fluid cace-main-container">
	<div class="row">
		<div class="col">
			<?php for ($i=1; $i <= 10 ; $i++) { 
				$linkEmpresa = "#";
			?>
			<article class="profesional">
				<div class="row">
					<div class="col-md-4">
						<a href="<?php echo $linkEmpresa; ?>"><img src="https://dummyimage.com/110x110/cccccc/fff.png" align="" alt="Empresa nombre" class="float-left mr-4" /></a>
						<h3><a href="<?php echo $linkEmpresa; ?>">Categoría</a></h3>
						<h1><a href="<?php echo $linkEmpresa; ?>">Nombre Profesional</a></h1>
					</div>
					<div class="col-md-3">
						<ul class="list-unstyled fa-ul contacto">
						  <li><i class="fa-li fa fa-phone"></i>011 1562 1212</li>
						  <li><i class="fa-li fa fa-home"></i>Carlos Pelegrini 651 4º</li>
						  <li><i class="fa-li fa fa-globe"></i>www.nasoft.com.ar</li>
						  <li><i class="fa-li fa fa-envelope-o"></i>correo@nasoft.com.ar</li>
						</ul>
					</div>
					<div class="col-md-5">
						<div class="descripcion">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sollicitudin nec nisi ac commodo. Maecenas id purus et quam porta eleifend. Vestibulum scelerisque volutpat felis a condimentum. </p>
							<a href="<?php echo $linkEmpresa; ?>" class="btn-mas">Ver más <i class="fa fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</article>
			<?php } ?>
		</div>
		<div class="sidebar col-md-3">
			<div class="sidebar-inner">
				<div class="wpr-categorias">
					<h3>Categorías</h3>
					<ul class="list-unstyled list-categorias">
					<?php for ($i=0; $i < 5; $i++) { ?>
						<li><a class="btn-categoria" href="#">Categoria</a></li>
					<?php } ?>
					</ul>
					<a class="btn btn-block btn-seconday btn-gris mb-4" href="#">ver más</a>
					<a class="btn btn-block btn-celeste  mb-1" href="#">registrate</a>
					<a class="btn btn-block btn-transparente" href="#">inicia sesión</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();