<div class="container-fluid cace-main-container">
	<div class="wpr-registro-inicio">
		<div class="row">
			<div class="col col-md-6 col-12">
				<form class="form" action="<?php echo wp_login_url(home_url()); ?>" method='post'>
					<h2>INICIA SESIÓN</h2>	
					<?php if ($resultFail != false) { ?>
					<div class="alert alert-danger">
						<p>Hemos encontrado un error al intertar loguearnos. Por favor, intenta nuevamente.</p>
					</div>
					<?php } ?>
					<div class="form-group">
						<input type="text" class="form-control" id="user_login" name="log" placeholder="Tu email" value="<?php echo $username; ?>">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="user_password" name="pwd" placeholder="Contraseña">
					</div>
					<div class="form-group mt-4">
						<button name="wp-submit" type="submit" class="btn btn-default btn-celeste btn-block">ingresar</button>
					</div>
					<a href="#" class="btn btn-block btn-link">¿No recordás tu contraseña?</a>
					<input type="hidden" name="recirect_to" value="<?php home_url(); ?>">
				</form>
			</div>
			<div class="col-md-6 col-12">
				<form>
					<h2>VALIDACIÓN EXITOSA!</h2>	
					<div class="alert alert-success">
	                  <p >Tu usuario ha sido validado exitosamente<BR/>
	                  Ahora ya puedes <a href="<?php get_page_url('ingreso'); ?>">Iniciar sesión</a></p>
	                </div>
	               </form>
			</div>
		</div>		
	</div>
</div>